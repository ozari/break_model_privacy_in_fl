import torch


def accuracy(preds, y):
    """
    :param preds:
    :param y:
    :return:
    """
    prediction_prob = torch.softmax(preds, dim=1)
    _, predicted = torch.max(prediction_prob, 1)
    correct = (predicted == y).float()
    acc = correct.sum() / len(correct)
    return acc
