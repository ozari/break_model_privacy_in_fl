from .sythetic import get_iterator_synthetic
from .adult import get_iterator_adult, get_iterator_purchase100
from .cifar10 import get_iterator_cifar10


def get_iterator(name, path, device, batch_size, dp, test_dataset):
    if name == "synthetic":
        return get_iterator_synthetic(path, device, batch_size, dp, test_dataset)
    elif name == "adult":
        return get_iterator_adult(path, device, batch_size = batch_size)
    elif name == "purchase_100":
        return get_iterator_purchase100(path, device, batch_size = batch_size)
    elif name == "cifar10":
        return get_iterator_cifar10(path, device, batch_size, dp, test_dataset)
    else:
        raise NotImplementedError
