import torch
import json
from torch.utils.data import Dataset, DataLoader
from opacus.utils.uniform_sampler import UniformWithReplacementSampler


class Cifar10Dataset(Dataset):
    def __init__(self, json_file, device):
        self.device = device

        with open(json_file, "r") as f:
            data = json.load(f)

        self.X = torch.tensor(data["x"]).to(device)
        self.y = torch.tensor(data["y"]).to(device)

        self.num_classes = 10
        self.dimension = self.X.shape[1]

    def __len__(self):
        return self.X.shape[0]

    def __getitem__(self, idx):
        return self.X[idx], torch.unsqueeze(self.y[idx], 0)


def get_iterator_cifar10(file_path, device, batch_size, dp, test_dataset=False):

    dataset = Cifar10Dataset(file_path, device)
    # added bool test_dataset to prevent using Poisson sampler in testing
    if not dp or test_dataset:
        iterator = DataLoader(dataset, shuffle=True, batch_size=batch_size)
    else:
        iterator = DataLoader(dataset, batch_sampler=UniformWithReplacementSampler(num_samples=len(dataset),
                                                                                   sample_rate=batch_size / len(dataset)))
    return iterator
