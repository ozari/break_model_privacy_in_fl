import json
import numpy as np


if __name__ == '__main__':

    numbers = []
    with open('all_data.json', 'rb') as f:
        data = json.load(f)

    for user_data in data['user_data'].values():
        print(user_data['cluster_id'])

    for i in range(5):
        with open('train/' + str(i) + ".json", 'rb') as f:
            data = json.load(f)
            print(i, len(data['y']), sum(data['y']))
        numbers.append(len(data['y']))
    numbers = np.asarray(numbers)
    print(
        f"Sum {np.sum(numbers)} Mean {np.mean(numbers)} std {np.sqrt(np.var(numbers))} Min {np.min(numbers)} Max {np.max(numbers)}")
