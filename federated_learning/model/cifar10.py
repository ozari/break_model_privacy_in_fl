import torch
import torch.nn as nn
from ..utils.optim import get_lr_scheduler, get_optimizer
from .model import Model
from opacus import PrivacyEngine


class Cifar10_model(nn.Module):
    def __init__(self, **_):
        super().__init__()
        self.layer_list = nn.ModuleList([
            nn.Sequential(nn.Conv2d(3, 32, (3, 3), padding=1, stride=(1, 1)), nn.ReLU()),
            nn.Sequential(nn.Conv2d(32, 32, (3, 3), padding=1, stride=(1, 1)), nn.ReLU()),
            nn.AvgPool2d(2, stride=2),
            nn.Sequential(nn.Conv2d(32, 64, (3, 3), padding=1, stride=(1, 1)), nn.ReLU()),
            nn.Sequential(nn.Conv2d(64, 64, (3, 3), padding=1, stride=(1, 1)), nn.ReLU()),
            nn.AvgPool2d(2, stride=2),
            nn.Sequential(nn.Conv2d(64, 128, (3, 3), padding=1, stride=(1, 1)), nn.ReLU()),
            nn.Sequential(nn.Conv2d(128, 128, (3, 3), padding=1, stride=(1, 1)), nn.ReLU()),
            nn.AvgPool2d(2, stride=2),
            nn.Sequential(nn.Conv2d(128, 256, (3, 3), padding=1, stride=(1, 1)), nn.ReLU()),
            nn.Conv2d(256, 10, (3, 3), padding=1, stride=(1, 1)),
        ])

    def forward(self, x):
        for layer in self.layer_list:
            x = layer(x)
        return torch.mean(x, dim=(2, 3))  # averaging over spatial dimensions

    def weight_reset(m):
        if isinstance(m, nn.Conv2d) or isinstance(m, nn.Linear):
            m.reset_parameters()

    def reset_parameters(self):
        for layer in self.layer_list:
            self.apply(Cifar10_model.weight_reset)

class Cifar10Model(Model):
    def __init__(self, criterion, metric, device, input_dimension, num_classes, iterator, args,
                 optimizer_name="sgd", lr_scheduler="constant", initial_lr=1e-3, epoch_size=1):
        super(Cifar10Model, self).__init__()

        self.criterion = criterion
        self.metric = metric
        self.device = device
        self.net = Cifar10_model().to(self.device)
        self.optimizer = get_optimizer(optimizer_name, self.net, initial_lr)

        if args.DP:
            self.sample_rate = min(args.bz / len(iterator.dataset), 1)

            self.privacy_engine = PrivacyEngine(
                self.net,
                sample_rate=self.sample_rate,
                alphas=[1 + x / 10.0 for x in range(1, 100)] + list(range(12, 64)),
                noise_multiplier=args.noise_mutliplier,
                max_grad_norm=args.max_grad_norm,
            )
            self.privacy_engine.attach(self.optimizer)
        self.lr_scheduler = get_lr_scheduler(self.optimizer, lr_scheduler, epoch_size)

    def fit_iterator_one_epoch(self, iterator):
        epoch_loss = 0
        epoch_acc = 0

        self.net.train()

        for x, y in iterator:
            self.optimizer.zero_grad()

            y = y.long().view(-1)

            predictions = self.net(x)

            loss = self.criterion(predictions, y)

            acc = self.metric(predictions, y)

            loss.backward()

            self.optimizer.step()
            self.lr_scheduler.step()

            epoch_loss += loss.item()
            epoch_acc += acc.item()

        return epoch_loss / len(iterator), epoch_acc / len(iterator)

    def fit_batch(self, iterator, update=True):
        self.net.train()

        x, y = next(iter(iterator))

        self.optimizer.zero_grad()

#        y = torch.tensor(y, dtype=torch.long, device=self.device).view(-1)
        y = y.long().view(-1)

        predictions = self.net(x)

        loss = self.criterion(predictions, y)

        acc = self.metric(predictions, y)

        loss.backward()

        if update:
            self.optimizer.step()
            self.lr_scheduler.step()

        batch_loss = loss.item()
        batch_acc = acc.item()

        return batch_loss, batch_acc

    def evaluate_iterator(self, iterator):
        epoch_loss = 0
        epoch_acc = 0

        self.net.eval()
        total = 0
        with torch.no_grad():
            for x, y in iterator:
                predictions = self.net(x)

                y = y.long().view(-1)

                loss = self.criterion(predictions, y)

                acc = self.metric(predictions, y)

                epoch_loss += loss.item()*len(y)
                epoch_acc += acc.item()*len(y)
                total += len(y)

        return epoch_loss / total, epoch_acc / total
