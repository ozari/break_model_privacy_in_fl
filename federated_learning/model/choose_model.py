import torch
import torch.nn as nn
from .synthetic import LinearModel
from .neural_network import NeuralNetwork
from .cifar10 import Cifar10Model
from ..utils.metric import accuracy


def get_model(name, device, iterator, args, epoch_size, optimizer_name="sgd", lr_scheduler="constant",
              initial_lr=1e-3, seed=1234, coeff=1):
    """
    Load Model object corresponding to the experiment
    :param name: experiment name; possible are: synthetic, shakespeare, sent140, inaturalist, femnist
    :param device:
    :param iterator: torch.utils.DataLoader object representing an iterator of dataset corresponding to name
    :param epoch_size:
    :param optimizer_name: optimizer name, for now only "adam" is possible
    :param lr_scheduler:
    :param initial_lr:
    :param seed:
    :param coeff
    :return: Model object
    """

    torch.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    if name == "linear":
        input_dimension = iterator.dataset.dimension
        num_classes = iterator.dataset.num_classes
        metric = accuracy
        criterion = nn.CrossEntropyLoss()
        # added iterator as an argument, to be used to get the sample rate
        return LinearModel(criterion, metric, device, input_dimension, num_classes, iterator, args,
                           optimizer_name, lr_scheduler, initial_lr, epoch_size)
    elif name == "cifar10":
        input_dimension = iterator.dataset.dimension
        num_classes = iterator.dataset.num_classes
        metric = accuracy
        criterion = nn.CrossEntropyLoss()
        return Cifar10Model(criterion, metric, device, input_dimension, num_classes, iterator, args,
                           optimizer_name, lr_scheduler, initial_lr, epoch_size)


    elif name == "neural":
        input_dimension = iterator.dataset.dimension
        num_classes = iterator.dataset.num_classes
        metric = accuracy
        criterion = nn.CrossEntropyLoss()
        return NeuralNetwork(criterion, metric, device, input_dimension, num_classes,
                           optimizer_name, lr_scheduler, initial_lr, epoch_size)
    else:
        raise NotImplementedError
