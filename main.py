from federated_learning.federated_learning_process import FederatedLearningFramework
from utils.args import parse_args
from adversary.adversary import Adversary
from attack.attacker import Insider_attack

if __name__ == '__main__':
    args = parse_args()

    adversary = Adversary(args)

    if not adversary.check_if_data_exists():
        FL_Process = FederatedLearningFramework(args)
        FL_Process.launch()

    adversary.settle_for_decode_evaluation()

    if not adversary.check_if_result_exists():
        adversary.check_other_benchmark()
        print("----------- Phase I: simulates the gradient networks for workers --------------------")
        if args.adversary_ability != "personalized_attack":
            adversary.train_gradient_network()

    print("----------- Phase II: decoding the local optimum for workers --------------------")
    adversary.decode_local_models()
    adversary.save_results()

    attacker = Insider_attack(args)
    attacker.print_extraction_performance()

    if args.experiment == "synthetic":
        attacker.attack_pair(added_test_local_data=False, even_test_data=True)
    elif args.experiment == "adult" or args.experiment == "purchase_100":
        attacker.attack_pair(added_test_local_data=False, even_test_data=False)
    else:
        raise NotImplementedError
    attacker.save_results()
    """
    #attacker.attack_chosen([2,3], added_test_local_data=False, even_test_data=False)
    #attacker.attack_all(added_test_local_data=False, even_test_data=True)
    attacker.plot_attacker_performance_paper()
    #attacker.plot_embedding_optimum_model()
    #attacker.interpret_models()
    #attacker.plot_data_point_probability([2,7])
    """
