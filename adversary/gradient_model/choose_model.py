from .model import LinearNet


def get_gradient_model(model, input_size, num_features):
    if model == "nn_linear":
        return LinearNet(input_size, num_features)
    else:
        raise NotImplementedError