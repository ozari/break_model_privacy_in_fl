import json
import os
import torch
import torch.nn as nn
from federated_learning.model.neural_network import NeuralLayer
from federated_learning.model.synthetic import LinearLayer
from adversary.gradient_model.choose_model import get_gradient_model

def get_local_model_structure(model, local_data_dir):
    if model == "linear" or model == "neural":
        filepath = os.path.join(local_data_dir, "train", "0.json")
        with open(filepath, 'rb') as f:
            data = json.load(f)

        num_dimension = torch.tensor(data["x"]).shape[1]
        num_classes = data["num_classes"]
        if model == "linear":
            return LinearLayer(num_dimension, num_classes), num_classes, num_dimension
        if model == "neural":
            return NeuralLayer(num_dimension, 256, num_classes), num_classes, num_dimension
    else:
        raise NotImplementedError


def map_vector_to_net(adversary_local_model, net, num_classes, num_dimension, model):
    if model == "linear":
        iter = 0
        for param in net.parameters():
            if iter == 0:
                to_fix = adversary_local_model[:-num_classes]
                param.data = to_fix.reshape(num_classes, num_dimension)
            else:
                param.data = adversary_local_model[-num_classes:]
            iter += 1
    else:
        raise NotImplementedError

def map_vector_to_gradient_net(adversary_local_model, gradient_network_type, input_size, num_features):
    adversary_local_model = torch.tensor(adversary_local_model)
    net = get_gradient_model(gradient_network_type, input_size, num_features)
    num_to_count = num_features*input_size
    start = 0
    for index, param in enumerate(net.parameters()):
        if index == 0:
            end = start+num_to_count
            param.data = adversary_local_model[start:end].reshape(num_features, input_size)
            start = end
        elif index == 1:
            end = start+num_features
            param.data = adversary_local_model[start:end]
            start = end
        elif index == 2:
            end = start+num_to_count
            param.data = adversary_local_model[start:end].reshape(input_size, num_features)
            start = end
        elif index == 3:
            param.data = adversary_local_model[start:]
    return net

def map_net_to_vector(model_worker, type="learning_network"):
    if type == "learning_network":
        iter_num = 0
        for param in model_worker.net.parameters():
            if iter_num == 0:
                train_model = param.data.clone().view(-1)
            else:
                train_model = torch.cat((train_model, param.data.clone().view(-1)))
            iter_num += 1
    elif type == "gradient_network":
        iter_num = 0
        for param in model_worker.parameters():
            if iter_num == 0:
                train_model = param.data.clone().view(-1)
            else:
                train_model = torch.cat((train_model, param.data.clone().view(-1)))
            iter_num += 1
    else:
        raise NotImplementedError
    return train_model