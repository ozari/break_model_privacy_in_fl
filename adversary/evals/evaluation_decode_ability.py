import torch.nn as nn
import torch
import os
import json
from federated_learning.utils.metric import accuracy
from .get_local_model_structure import get_local_model_structure, map_vector_to_net


def evaluate_decoded_model_ability(adversary_local_model, device, worker_id, data_test, data_directory, model):
    net, num_classes, num_dim = get_local_model_structure(model, data_directory)
    map_vector_to_net(adversary_local_model, net, num_classes, num_dim, model)
    net.to(device)

    filepath: str = os.path.join(data_directory, "train", str(worker_id) + '.json')
    with open(filepath, 'rb') as f:
        data_train = json.load(f)
    x = torch.FloatTensor(data_train['x']).to(device)
    y = torch.LongTensor(data_train['y']).view(-1).to(device)
    criterion = nn.CrossEntropyLoss()
    prediction = net(x)
    train_acc = accuracy(prediction, y)
    loss = criterion(prediction, y)
    x = torch.FloatTensor(data_test[0]).to(device)
    y = torch.LongTensor(data_test[1]).view(-1).to(device)
    prediction = net(x)
    test_acc = accuracy(prediction, y)
    return loss.item(), train_acc.item(), test_acc.item()


def extraction_acc(extract_model, optimum_model, device, data_test, data_directory, model):
    net_extract, num_classes, num_dim = get_local_model_structure(model, data_directory)
    map_vector_to_net(extract_model, net_extract, num_classes, num_dim, model)
    net_extract.to(device)

    net_optimum, num_classes, num_dim = get_local_model_structure(model, data_directory)
    map_vector_to_net(optimum_model, net_optimum, num_classes, num_dim, model)
    net_optimum.to(device)

    x = torch.FloatTensor(data_test[0]).to(device)
    prediction_extract = net_extract(x)
    prediction_optimum = net_optimum(x)
    _, predicted_optimum = torch.max(torch.softmax(prediction_optimum, dim=1), 1)
    acc = accuracy(prediction_extract, predicted_optimum)

    return acc.item()